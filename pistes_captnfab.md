# Pistes pour la rédaction des flyers

Plusieurs aspects de ce que le grand public peut voir de **Debian**, méritent
d'être détailés. Mais bien sûr, tous ne peuvent pas l'être.

Voici une sélection, qui vaut ce qu'elle vaut.

Je pense qu'en se basant sur l'esprit de chacun (quitte à en supprimer un ou
deux) des paragraphes, et en créant quelques illustrations, il est possible de
composer un flyer

## La distribution

Debian est une **Distribution GNU/Linux**, c'est à dire un système complet
permettant d'utiliser un ordinateur, ses différents périphériques, et d'en
faire ce que l'on veut grâce à une collection de plusieurs dizaines de milliers
d'applications, de jeux, d'outils, de documentations, etc.

Debian ne développe pas tous ces logiciels mais les intègre au système sous la
forme de **paquets** que vous pouvez installer à tout moment.

## Le logiciel libre

À moins que votre matériel ne nécessite quelques pilotes propriétaires, vous
pouvez utiliser une Debian composée à 100% de logiciels libres.

Un logiciel libre fait un utilisateur libre d'examiner, modifier, utiliser et
partager ce logiciel. C'est un gage d'éthique et de transparence.

## Le projet

Debian est un projet démocratique soutenu par un contrat social, aussi
fondamental que la constitution d'un pays. Il pose le logiciel libre, la
transparence et la qualité comme des valeurs fondamentales.

Les membres du projet Debian, les **Développeurs Debian**, s'occupent de la
maintenance des différents paquets et services avec l'aide des autres
contributeurs. Ils ont en plus la possibilité de voter ou de prendre des
responsabilités dans le projet.

## Les versions de Debian

Debian est en développement constant, les logiciels qu'elle intègre sont sans
cesse mis à jour. Afin de proposer un environnement logiciel cohérent et
robuste, Debian sort régulièrement une version dite **stable** dans lequel
aucun bug grave n'est connu, et dont le support de sécurité est assuré.

Ces versions portent un numéro et un nom de code issus des films d'animation
Toy Story. Exemple: Debian Buster 10.1

## Descendance et communauté

De nombreuses distributions (Ubuntu, Linux Mint, Kali, Tails, PureOS) sont des
Debian modifiées pour correspondre à certains besoins particuliers, mettant
l'accent sur des valeurs différentes.

Cette communauté d'utilisateurs de distributions forme une grande famille dans
laquelle l'entraide et le partage permette l'appropriation par chacun de
l'outil informatique.
