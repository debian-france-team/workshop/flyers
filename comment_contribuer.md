# Face 1

Obtenir de l'aide

- Documentation : https://www.debian.org/doc
- Assistance : https://www.debian.org/support
- Être informé des actualités : https://lists.debian.org/debian-news-french/
- Groupe des utilisateurs : https://lists.debian.org/debian-user-french/
- Wiki Debian : https://wiki.debian.org/

« Le contrat social Debian » et « Les principes du logiciel libre selon Debian » : https://www.debian.org/social_contract

# Face 2

Contribuer à Debian

- Comment contribuer : https://www.debian.org/intro/help
- Accueil des nouveaux arrivants : https://wiki.debian.org/Welcome/Contributors
- Debian Mentors : https://wiki.debian.org/Mentors
- Contribuer au wiki : https://wiki.debian.org/Teams/Welcome/WikiEditors
- Participer à la traduction : https://www.debian.org/international/french
- Suggestion de contribution : https://wiki.debian.org/how-can-i-help

[Premier essai](https://cloud.debian.net/s/help-refcard)